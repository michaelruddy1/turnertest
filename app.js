/**
 * @author Michael Ruddy
 * @description: Turner Challenge
 * @version: 0.0.1
 **/
var connectionString = 'mongodb://readuser:reader1234@SG-mssmongodev02-874.servers.mongodirector.com:27017/dev-test';

var express = require('express');
var http = require('http');
var path = require('path');
var _ = require('underscore');
var app = express();

var mongoose = require('mongoose');
var connection = null;
var isConnected = false;
var titleModel = null;


/**
 * Database connection sequence
 **/
mongoose.connect(connectionString);
connection = mongoose.connection;
titleModel = mongoose.model('titles', mongoose.Schema());

connection.on('error', function () {
  isConnected = false;
});

connection.on('open', function () {
  isConnected = true;
});


/**
 * GET endpoint for Titles
 **/
app.get('/title', function (req, res) {
	var filterParams = req.query.filter ? req.query.filter.split(',') : null;
	var searchParams = req.query;
	delete searchParams.filter;

  if (!isConnected) {
    res.status(500);
    res.json({
      error: 'I think you broke something..'
    })
  }

  titleModel.find(searchParams).lean().exec(function (err, results) {
    if (err) {
      res.status(400);
      res.json({
        error: 'Bad request!'
      })
    }

    if(filterParams) {
  	  results = _.map(results, function(title) {
  	  	return _.pick(title, filterParams);
  	  });
  	}

  	res.json(results)

  });
});


/**
 * Configure HTTP
 **/
app.use(express.static(path.join(__dirname, 'public')));
app.set('port', process.env.PORT || 3000);

http.createServer(app).listen(app.get('port'), function () {
  console.log('Code Challenge running on port: ' + app.get('port'));
});
