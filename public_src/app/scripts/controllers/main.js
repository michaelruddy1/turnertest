'use strict';

/**
 * MainCtrl listens for user events and coordinates backend with views
 **/
angular.module('titlesApp')
  .controller('MainCtrl', ['$scope', '$http', '$routeParams', '$location',
    function ($scope, $http, $routeParams, $location) {
      $scope.cards = [];
      $scope.selectModel = null;
      $scope.icons = [{
        'icon': 'glyphicon-search',
        'background-color': '#18DD00'
      }, {
        'icon': 'glyphicon-send',
        'background-color': '#E1C829'
      }, {
        'icon': 'glyphicon-tower',
        'background-color': '#2FB5F3'
      }, {
        'icon': 'glyphicon-heart-empty',
        'background-color': '#FC82C3'
      }, {
        'icon': 'glyphicon-pushpin',
        'background-color': '#1E023F'
      }];

      $scope.updateIcon = function (cardIndex, icon) {
        $scope.cards[cardIndex] = icon;
      }

      //generate 5 random cards
      for (var i = 0; i < 5; i++) {
        $scope.cards.push(
          $scope.icons[Math.floor(Math.random() * $scope.icons.length)]
        );
      }
    }
  ]);